<?php
  // $city = 'hyeres';
  $city = 'toulon';
?>

<div class="page">
  <div class="page-inside grid">
    <div class="fond-<?= $city ?>"></div>
    <div class="lettrage">
      <div class="design"></div>
      <div class="parade"></div>
    </div>
    <!-- <div class="mjson"> -->
    <!-- </div> -->
    <!-- <div class="mjson2"></div> -->

  </div>
</div>

<link rel="stylesheet" type="text/css" href="files/cover/style.css?v<?php rand(); ?>" media="all">
<script charset="utf-8">
  $('.categorie li').arctext({radius: 5, dir: 1,rotate: false});
  $('.date span').arctext({radius: 30, dir: 21, rotate: false});
  $('.ville span').arctext({radius: 1, dir: -1, rotate: false});

  zoomPages($('.range'), $('.page'));
  var orderJson2 = ['timesb-handles-over','timesb-handles-dot-over'];
  // models.json = [];
  lettrine(
    '.lettrage .parade',
    'PARADE',
    10,
    {
      json: orderJson2,
      style: [],
      morph: {
        'transform': ['scale-y', '1', '0.5' ],
  },
    }
  );
  var orderJson1 = [
    'timesb-handles-over',
    'timesb-over',
    'timesb-handles-over',
    'timesb-over',
    'timesb-handles-dot-over',
  
  ] ;
  var value = [];
  lettrine(
    '.lettrage .design',
    'CONCOURS',
    3,
    {
      json: orderJson1,
      style: [],
      morph: {
        'transform': ['scale-y', '1', '0.5' ],
      },
    }
  );

  var point = 5;
  var blue = '#003DA5';
  var vert = '#5EC286';
  // var fond = '#FCCC93';
  //
  if ( '<?= $city ?>' == 'hyeres' ){
    var fond = '#EDE939';
    var tier = vert;
  }else if ( '<?= $city ?>' == 'toulon' ){
    var fond = '#F8B8CB';
    var tier = '#ffffff';
  }

  var font = 'parade';
  // L E F T 
  // 1A

    </script>
<script src="files/cover/mj-<?= $city ?>.js" ></script>

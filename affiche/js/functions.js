
function readhash(){
  var hash = location.hash.substr(1).split("-");
  return hash;
}

function writehash(docVal, zoomVal) {
  location.hash = '#'+docVal+'-'+zoomVal;
}

function zoomPages(range, content){
  var inputOnLoad = range.val();
  var hash = readhash();
  content.css({'transform': 'scale('+hash[1]+')'});
  document.addEventListener('input', function(e) {
    var value = e.target.value/10;
    if(e.target.className == 'rangeValue'){
      content.css({'transform': 'scale('+value+')'});
      writehash(hash[0], value);
    }
  })
}

function loadFiles(version){
  var hash = readhash();
  var pathContent = $('#content').attr('data-content');
  $('#content').load(pathContent);
  var rdm = Math.random();
  $('.content').load('files/'+ hash[0] +'/index.php#'+ rdm)
}

function layout(){
 var allLayout = $("[class^⁼'layout-']");
}

function lettrine(container, chaineStr, repeat, models){
  function getSvg(data, style, morph) {
      var value;
      var chaineSpl = chaineStr.split('');
      value = '<div class="word" id="word_' + chaineStr + '">';
    $.getJSON('json-lettrage/'+ data +'.json', function(font){

      var h = font.height;
      chaineSpl.forEach(function(letter){
        for (var i = 0, len = (font.chars.length - 1); i < len; i++) {
          if (letter.charCodeAt() == font.chars[i].dec){
            var dec = font.chars[i].dec;
            var w = font.chars[i].width;
            var d = font.chars[i].d;
            var m = morph;
            var p = '<path d="'+ d +'" style="'+ style +'"></path>';
            value += '<svg class="letter" id="letter_' + dec + '" viewbox="0 0 '+ w +' '+ h +'">' + p + '</svg>';
          }
        }
      })
      value += '</div>';
      for (var y = 0, len = repeat; y < len; y++) {
        if(container == '.lettrage .parade'){
          $(container).append(value);
        }else{
          $(container).prepend(value);
        }
      }

    })

  }
  var increm = 0;
  // models.json.forEach(function(index){
  //   console.log(index);
  // })
  for (var u = 0, len = models.json.length; u < len; u++) {
    getSvg(models.json[u], models.style[u], models.morph);
  }
};


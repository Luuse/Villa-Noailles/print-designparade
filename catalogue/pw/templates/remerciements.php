  <div class="singleContent remerciements <?= $partie->id ?>">
    <div class="content logo content0" id="content<?= $partie->id ?>">
      <?php foreach ($images as $image): ?>
          <?php if ($print == true) { ?>
            <img src="<?= $image->url ?>" />
          <?php } elseif ($print == false) { ?>
            <img src="<?= $image->size(400, 0)->url ?>" />
          <?php } ?>
      <?php endforeach; ?>
    </div>
    <div class="content1">
      <div class="text">
        <div class="columnFr column texte_fr"><?= rft($texteFr) ?></div>
      </div>
    </div>
  </div>
</section>

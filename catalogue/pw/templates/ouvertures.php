  <div class="singleContent ouvertures <?= $partie->id ?>">
    <div class="content content0" id="content<?= $partie->id ?>">
      <img src="<?= $images->first->url ?>" alt="">
    </div>
    <div class="content1">
      <div class="titles">
        <h1 class="titre"><?= $sous_titreFr ?></h1>
      </div>
      <div class="sommaire">
        <div class="column"><?= $textFr ?></div>
      </div>
    </div>
  </div>
</section>

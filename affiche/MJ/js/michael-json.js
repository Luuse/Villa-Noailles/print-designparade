function MichaelJson(enter){
  var data       = [];
  var data       = enter.jsonFile;
  var container  = enter.container;
  var color      = enter.findColor;
  var locPix     = [];
  var glyphColor = enter.glyphColor;
  var glyph      = enter.glyphs;
  var valRotate  = enter.rotate;
  var translX    = enter.translateX;
  var translY    = enter.translateY;
  var incStyle   = enter.incStyle;
  var fontName   = enter.font;
  var textCell   = [];

  function indiv(s, options){
    var deplaX = Math.floor(Math.random()*enter.translateX) + 1;
    var deplaY = Math.floor(Math.random()*enter.translateY) + 1;
    if (enter.translateX !== 0){
      deplaX *= Math.floor(Math.random()*2) == 1 ? 1 : -1;
    }else {
      deplaX = 0;
    }
    if (enter.translateY !== 0){
      deplaY *= Math.floor(Math.random()*2) == 1 ? 1 : -1;
    }else {
      deplaY = 0;
    }
    var posX = options.x + deplaX;
    var posY = options.y + deplaY;

    if(glyphColor[0] == 'auto'){
      glClor = options.colr;
    } else {
      var idcolor = Math.floor((Math.random() * glyphColor.length));
      glClor = glyphColor[idcolor];
    }

    var idglyph = Math.floor((Math.random() * glyph.length));
    var rotate = Math.floor((Math.random() * valRotate));

    var spanStyle = function(){
      var style = [];
      for (var s = 0, len = options.style.length; s < len; s++) {
        style.push(' '+options.style[s][0]+': '+options.style[s][1]+options.style[s][2]);
      }
      style = style.toString().replace(',', ';');
      var finalStyle = 'font-family: '+fontName+'; position: relative; margin-top: '+posY+'px; margin-left:' + posX + 'px; color: '+glClor+';fill: ' + glClor + '; transform-origin: 0 0; transform: rotate(' + rotate + 'deg); ' + style;
      return finalStyle;
    }

    var cellText = $('<text/>', {
      class: s,
      'style': spanStyle, 
      html: glyph[idglyph]
    }).attr({
      x: posX,
      y: posY,
      transform: 'rotate('+rotate+', '+posX+', '+posY+')',
  });
    return cellText[0].outerHTML;
  }

  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  var refresRand = parseInt(getRandomArbitrary(1,100000))
  $.getJSON('MJ/json/' + data + '.json?'+refresRand, function(image){
   
    var imWidth  = image.width;
    var imHeight = image.height;
    var pixels   = image.pixels;
    if (enter.scan[0] == 'all'){
      var scanMin = 0;
      var scanMax = imHeight; 
    }else{
      var scanMin = enter.scan[0];
      var scanMax = enter.scan[1]; 
    }
    var min = scanMin * imWidth;
    var max = scanMax * imWidth;

    for (var i = min, len = pixels.length; i < max; i++) {
      var u = 0;

      if(pixels[i].pix[0] == color || 'all' == color ) {
        var randFreq  = Math.floor(Math.random()* enter.freq[1]);
        var interFreq = enter.freq[1] - enter.freq[0];
        if (randFreq >= interFreq ){
          locPix.push([pixels[i].pix[0], pixels[i].pix[1], pixels[i].pix[2]]);
          u++;
        }
      }

    }

    locPix.forEach(function(item, index){

      textCell += indiv('cellule', {
        colr: locPix[index][0],
        x: locPix[index][1],
        y: locPix[index][2],
        style: incStyle
      });

      enter.translateX += enter.incTransX;
      enter.translateY += enter.incTransY;

      for (var a = 0, len = incStyle.length; a < len; a++) {
        incStyle[a][1] += incStyle[a][3];
      }
    })

    if ($(container + ' .page-mj').length == 0) {

      $(container).html('<svg  viewbox="0 0 ' + imWidth + ' ' + imHeight + '" class="page-mj" ></svg>');
      $(container +' .page-mj').html('<g>' +textCell+ '</g>');

    } else if($(container + ' .page-mj ').length != 0){

      var contSvg = $(container + ' .page-mj').html();
      $(container + ' .page-mj').html(contSvg +'<g>' +textCell+ '</g>');

    }

  });
}

////////

function SaveAsFile(t,f,m) {
  try {
    var b = new Blob([t],{type:m});
    saveAs(b, f);
  } catch (e) {
    window.open("data:"+m+"," + encodeURIComponent(t), '_blank','');
  }
}

function SvgSave(){
  var containerName = $('input.save').attr('data-container');
  var svgContent = $(containerName).html();
  var d = new Date();
  // var data = window.location.hash.substring(1);
  SaveAsFile(svgContent, 'MJ--'+d.getTime() + ".svg","text/plain;charset=utf-8");
}


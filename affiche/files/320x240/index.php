<div class="page">
  <div class="page-inside grid">
    <div class="lettrage">
      <div class="design"></div>
      <div class="parade"></div>
    </div>
    <div class="flow-hyeres"></div>
    <div class="flow-toulon"></div>
    <div class="flow-date"></div>
    <div class="flow-inauguration"></div>
    <div class="flow-categorie"></div>
    <div class="flow-signature"></div>
    <div class="flow-luuse"></div>
    <div class="flow-footer"></div>

    <div class="mjson">
    </div>

  </div>
</div>

<link rel="stylesheet" type="text/css" href="files/320x240/style.css?v<?php rand(); ?>" media="all">
<script charset="utf-8">
  zoomPages($('.range'), $('.page'));

  lettrine(
    '.lettrage .design',
    'DESIGN',
    2,
    {
      json: ['timesb', 'timesb-handles-dot', 'timesb-handles', ],
      style: ['stroke-width: .3px', 'stroke-width: .3px', 'stroke-width: .3px'],
      morph: {
        'transform': ['scale-y', '1', '0.5' ],
  },
    }
  );
  lettrine(
    '.lettrage .parade',
    'PARADE',
    2,
    {
      json: ['timesb', 'timesb-handles', 'timesb-handles-dot'],
      style: ['stroke-width: .3px', 'stroke-width: .3px', 'stroke-width: .3px'],
      morph: {
        'transform': ['scale-y', '1', '0.5' ],
  },
    }
  );
  // lettrine(
  //   '.lettrage .parade',
  //   'PARADE',
  //   1,
  //   {
  //     json: ['timesb', 'timesb-handles', 'timesb-handles-dot'],
  //     style: ['stroke-width: .3px', 'stroke-width: .3px', 'stroke-width: .3px']
  // }
  // );

  MichaelJson({
    jsonFile: 'doubleface',
    container: '.mjson',
    font: 'parade',
    glyphs: ['f', 'p', 'i', 'h'],
    findColor: '#ffffff',
    glyphColor: ['#02c47e', '#ff3cc8'],
    freq: [1,5],
      rotate: 30,
      translateX: 0,
      incTransX: 0.0005,
      translateY: 1,
      incTransY: 0.0005,
    scan: ['all'], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', 5, 'px', 0],
    ]
  });

  // MichaelJson({
  //   jsonFile: 'rectoverso',
  //   container: '.mjson',
  //   font:'parade',
  //   glyphs: ['f', 'p', 'i', 'h'],
  //   findColor: '#ffffff',
  //   glyphColor: ['yellow', '#3ee4be'],
  //   freq: [1,5],
  //   rotate: 30,
  //   translateX: 0,
  //   incTransX: 0.0005,
  //   translateY: 1,
  //   incTransY: 0.0005,
  //   scan: ['all'], // interval example ´scan: [10, 50]´
  //   incStyle: [
  //     ['font-size', 5, 'px', 0],
  //   ]
  // });

</script>
 

<?php

    $regex = array(

    	# input		        # output	                              # comment

                                                                # On commence par retirer tout les espaces avant de mettre les espaces fines
    	"/ ;/"	  =>	    ';',	                                  # espace point-virgule		            => point-virgule
      "/« /"	  =>	    '«',	                                  # guillement français ouvrant espace  => guillement français ouvrant
      "/« /"	  =>	    '«',	                                  # guillement français ouvrant espace  => guillement français ouvrant
      "/“ /"	  =>	    '«',	                                  # guillement français ouvrant espace  => guillement français ouvrant
    	"/“ /"	  =>	    '«',	                                  # guillement français ouvrant espace  => guillement français ouvrant
      "/ »/"	  =>	    '»',	                                  # espace guillement français fermant  => guillement français fermant
      "/ »/"	  =>	    '»',	                                  # espace guillement français fermant  => guillement français fermant
      "/ ”/"	  =>	    '»',	                                  # espace guillement français fermant  => guillement français fermant
    	"/ ”/"	  =>	    '»',	                                  # espace guillement français fermant  => guillement français fermant
      "/ :/"	  =>	    ':',	                                  # espace deux points	                => deux points
    	"/ :/"	  =>	    ':',	                                  # espace deux points	                => deux points
      "/ !/"	  =>	    '!',	                                  # espace point d'exclamation	        => point d'exclamation
    	"/ !/"	  =>	    '!',	                                  # espace point d'exclamation	        => point d'exclamation
      "/ \?/"	  =>	    '?',	                                  # espace point d'interrogation	      => point d'interrogation
    	"/ \?/"	  =>	    '?',	                                  # espace point d'interrogation	      => point d'interrogation

                                                                # Caractères spéciaux
    	"/& /"	  =>	    '&amp; ',	                              # &			                             => &amp;
    	"/\.\.\./"=>	    '&#8230;',	                            # AE			                           => Æ
    	//"/-/"	    =>	    '&#8209;',	                          # tiret			                         => tiret insécable

      # Espace insécable après les mots de 1 et 2 lettre pour le drapeau
      "/ (\b\p{L}{1,2}\b) /u"	      =>	    ' $1&nbsp;',        # cherche tout les mots de 1 et 2 lettre, accentués compris
      "/&nbsp;(\b[a-z]{1,2}\b) /i"	=>	    '&nbsp;$1&nbsp;',    # dans le cas où 2 mots de 1 ou 2 lettres se suivent

      # Espaces insécables the / and / our / for
      "/ the /" => ' the&nbsp;',
      "/ and /" => ' and&nbsp;',
      "/ our /" => ' our&nbsp;',
      "/ for /" => ' for&nbsp;',
    );

?>

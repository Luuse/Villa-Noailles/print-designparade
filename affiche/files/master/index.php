<?php
  // $city = 'hyeres';
  $city = 'toulon';
?>

<div class="page">
  <div class="page-inside grid">
    <div class="fond-<?= $city ?>"></div>
    <div class="lettrage">
      <div class="design"></div>
      <div class="parade"></div>
    </div>
    <div class="flow-titredp"></div>
    <div class="flow-titredp2"></div>
    <div class="flow-date-<?= $city ?>"></div>
    <div class="flow-inauguration"></div>
    <div class="flow-categorie"></div>
    <div class="flow-<?= $city ?>"></div>
    <div class="flow-information-<?= $city ?>"></div>
    <div class="flow-site"></div>
    <div class="flow-signature"></div>
    <div class="flow-luuse"></div>
    <div class="flow-footer-<?= $city ?>"></div>
    <div class="mjson">
    </div>
    <div class="mjson2"></div>

  </div>
</div>

<link rel="stylesheet" type="text/css" href="files/master/style.css?v<?php rand(); ?>" media="all">
<script charset="utf-8">
  // $('.categorie li .circleinf').arctext({radius: 50, dir: -1,rotate: true});
  $('.categorie li').arctext({radius: 5, dir: 1,rotate: false});
  $('.date span').arctext({radius: 30, dir: 21, rotate: false});
  // $('.inauguration').arctext({radius: 3, dir: 40,rotate: false});
  $('.ville span').arctext({radius: 1, dir: -1, rotate: false});

  zoomPages($('.range'), $('.page'));
  var orderJson2 = ['timesb-handles-over','timesb-handles-dot-over'];
  // models.json = [];
  lettrine(
    '.lettrage .parade',
    'PARADE',
    2,
    {
      json: orderJson2,
      style: [],
      morph: {
        'transform': ['scale-y', '1', '0.5' ],
  },
    }
  );
  var orderJson1 = ['timesb-handles-over','timesb-handles-dot-over','timesb-over','timesb-handles-dot-over'] ;
  var value = [];
  lettrine(
    '.lettrage .design',
    'DESIGN',
    2,
    {
      json: orderJson1,
      style: [],
      morph: {
        'transform': ['scale-y', '1', '0.5' ],
      },
    }
  );

  var point = 7;
  var blue = '#003DA5';
  var vert = '#5EC286';
  // var fond = '#FCCC93';
  //
  if ( '<?= $city ?>' == 'hyeres' ){
    var fond = '#EDE939';
    var tier = vert;
  }else if ( '<?= $city ?>' == 'toulon' ){
    var fond = '#FFB3BB';
    var tier = '#ffffff';
  }

  var font = 'parade';
  //// C E N T E R
  MichaelJson({
    jsonFile:'echant',
    container: '.mjson',
    font: font,
    glyphs: ['W', 'Y', 'v'],
    findColor: '#020202',
    glyphColor: [ 'white'],
    freq: [1,5],
    rotate: 30,
    translateX: 10,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [0, 150], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  MichaelJson({
    jsonFile:'echant',
    container: '.mjson2',
    font: font,
    glyphs: ['Z', '['],
    findColor: '#020202', 
    glyphColor: [fond, 'white'],
    freq: [1,4],
    rotate: 0,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0.001,
    scan: [50, 100], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });

  // 2B
  MichaelJson({
    jsonFile:'echant',
    container: '.mjson',
    font: font,
    glyphs: [']', 'a'],
    findColor: '#020202',
    glyphColor: ['white', vert, 'white'],
    freq: [1,1],
    rotate: 0,
    translateX: 0,
    incTransX: 0.01,
    translateY: 0,
    incTransY: 0.01,
    scan: [150, 300], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });

  // 2B L2
  MichaelJson({
    jsonFile:'echant',
    container: '.mjson2',
    font: font,
    glyphs: ['}', 'b', 'c'],
    findColor: '#020202',
    glyphColor: [fond, vert, 'white', 'white'],
    freq: [1,1],
    rotate: 0,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [150, 300], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', 10, 'pt', 0],
    ]
  });

  MichaelJson({
    jsonFile:'echant',
    container: '.mjson',
    font: font,
    glyphs: ['e', 'x', 'f'],
    findColor: '#020202',
    glyphColor: [fond, 'white', 'white'],
    freq: [1,2],
    rotate: 0,
    translateX: 0,
    incTransX: 0.0005,
    translateY: 0,
    incTransY: 0,
    scan: [300, 450], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });


  MichaelJson({
    jsonFile:'echant',
    container: '.mjson',
    font: font,
    glyphs: ['g', 'h', 'm', 'j'],
    findColor: '#ff0202',
    glyphColor: [ vert, 'white'],
    freq: [1,1],
    rotate: 30,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [0, 150], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });

  // 1B
  MichaelJson({
    jsonFile:'echant',
    container: '.mjson',
    font: font,
    glyphs: ['k', '~', 'o', 'q'],
    findColor: '#ff0202',
    glyphColor: [fond, 'white',  'white'],
    freq: [1,1],
    rotate: 0,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [150, 300], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });

  MichaelJson({
    jsonFile:'echant',
    container: '.mjson',
    font: font,
    glyphs: ['z', 'r', '`', 's', 'i'],
    findColor: '#ff0202',
    glyphColor: [fond, 'white'],
    freq: [1,1],
    rotate: 0,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [300, 150], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });

  // 3A
  MichaelJson({
    jsonFile:'echant',
    container: '.mjson',
    font: font,
    glyphs: ['t', 'n', 'd', 'w',],
    findColor: '#0202ff',
    glyphColor: [vert, vert, fond],
    freq: [1,1],
    rotate: -10,
    translateX: 0,
    incTransX: 0,
    translateY: 70,
    incTransY: -0.005,
    scan: [0, 150], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });

  // 3B
  // PAS TOUCH
  MichaelJson({
    jsonFile:'echant',
    container: '.mjson',
    font: font,
    glyphs: ['y', '_', '{', '|'],
    findColor: '#0202ff',
    glyphColor: [vert, 'white'],
    freq: [1,1],
    rotate: 0,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [150, 300], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  MichaelJson({
    jsonFile:'echant',
    container: '.mjson',
    font: font,
    glyphs: ['À', 'p'],
    findColor: '#0202ff',
    glyphColor: [vert, 'white'],
    freq: [1,1],
    rotate: 0,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [150, 300], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });




    </script>

<?php $id = $partie->id; ?>
<div class="singleContent singleContent<?= $key ?> <?= $id ?>">

  <div class="content content0" id="content<?= $partie->id ?>">
    <div class="header">
      <h1 class="artist"><?= $artiste ?></h1>
      <h2 class="country">
        <span class="fr"> <?= $paysFr ?>  </span>
        <span class="en"><?= $paysEn ?></span>
      </h2>
      <h1 class="title"><?= $titre ?></h1>
    </div>
    <div class="text">
      <?php if ($id == 1044 || $id == 1062 || $id == 1073 || $id == 1079) { ?> <!-- CAROLIEN + Pasollunghi + ABN + ESAD -->
        <div class="columnFr column texte_fr"><?= rft($texteFr) ?>
          <h1 class="title"><?= $titreEn ?></h1>
          <span class="columnEn"><?= rft_en($texteEn) ?></span>
        </div>
      <?php } else { ?>
      <div class="columnFr column texte_fr"><?= rft($texteFr) ?></div>
        <!-- <div class="header"> -->
        <h1 class="title"><?= $titreEn ?></h1>
        <!-- </div> -->
      <div class="columnEn column texte_en"><?= rft_en($texteEn) ?></div>
      <?php } ?>
    </div>
  </div>

  <div class="content1">

  </div>

</div>

<div class="content affiche">
    <?php
    include("inc/imgs.php");
	 ?>
        <!-- DESIGN PARADE -->
        <div class="titre1">
            <div class="design"><span class="d">D</span><span class="e">E</span><span class="s">S</span><span class="i">I</span><span class="g">G</span><span class="n">N  </span> </div><br>
            <div class="parade"><span class="p">P</span><span class="a">A</span><span class="r">R</span><span class="a">A</span><span class="d">D</span><span class="e">E</span></div>
        </div>
        <div class="date">Du <span>28</span> juin au <span>1<sup>er</sup></span> Juillet 2018 </div>
        <div class="inauguration">Exposition</span> <span>jusqu'au</span> <span>30</span> <span>septembre</span></div>
            <!-- AFFICHE HYERES -->
      <!-- <div class="titre">
          <div class="design"><span class="d">D</span><span class="e">E</span><span class="s">S</span><span class="i">I</span><span class="g">G</span><span class="n">N  </span> </div>
          <div class="parade"><span class="p">P</span><span class="a">A</span><span class="r">R</span><span class="a">A</span><span class="d">D</span><span class="e">E</span></div>
      </div> -->
        <div class="hyere">
          <div class="ville">HYÈRES</div>
            <div class="domaine"> <span>13<sup>e</sup></span> Festival INTERNATIONAL</div>
            <div class="sousdomaine">Design</div>>
            <div class="lieu"><span>villa</span><br><span>Noailles </span></div>
            <!-- <div class="titre2">
          <div class="design"><span class="d">D</span><span class="e">E</span><span class="s">S</span><span class="i">I</span><span class="g">G</span><span class="n">N  </span> </div>
          <div class="parade"><span class="p">P</span><span class="a">A</span><span class="r">R</span><span class="a">A</span><span class="d">D</span><span class="e">E</span></div>
            </div> -->
            <ul class="information">
                <li class="exposition">Phillippe Malouin</li>
                <li class="exposition">Carolien Niebling</li>
                <li class="exposition">Arthur Hoffner</li>
                <li class="exposition" >Picasso</li>
                <li class="exposition">François Passolunghi & Joachim Jirou-Najou</li>
                <li class="exposition">Hotel La Reine Jane, 14 designers</li>
                <li class="exposition">Superpoly</li>
                <li class="exposition">concours : 10 designers</li>
            </ul>
        </div>

        <!-- AFFICHE TOULON -->

        <div class="toulon">
            <div class="ville">TOULON</div>
            <div class="domaine"> <span>3<sup>E</sup></span> Festival INTERNATIONAL<br> <span>Arc</span><span>hit</span><span>ect</span>ure d'in<span>té</span>r<span>ieur</span><br></div>
            -
            <div class="lieu">
                    <span>Ancien évêché</span><br><span> 69, cours Lafayette Toulon</span>
            </div>
            -
            <ul class="information">
                <li class="exposition">Pierre Yovanovitch</li>
                <li class="exposition">Pierre Marie</li>
                <li class="exposition">Lesage Intérieurs</li>
                <li class="exposition">Alexandre Benjamin Navet</li>
                <li class="exposition">Julien Oppenheim</li>
                <li class="exposition">Daragh Soden</li>
                <li class="exposition">Victor Levai & Mathilde Vallantin Dulac</li>
                <li class="exposition">Debeaulieu</li>
                <li class="exposition">5 Rooms, 5 designers</li>
                <li class="exposition">École supérieure d’art et de design Toulon Provence Méditerranée</li>
                <li class="exposition">concours : 10 architectes d'intérieur</li>
            </ul>
        </div>
        <ul class="categorie">
          <li>RENCONTRES</li>
          <li>FILMS</li>
          <li>MARCHÉ DU DESIGN</li>
          <li> CONCOURS</li>
          <li>EXPOSITIONS</li>
        </ul>
        <!-- <div class="titre1">
            <div class="design"><span class="d">D</span><span class="e">E</span><span class="s">S</span><span class="i">I</span><span class="g">G</span><span class="n">N  </span> </div>
            <div class="parade"><span class="p">P</span><span class="a">A</span><span class="r">R</span><span class="a">A</span><span class="d">D</span><span class="e">E</span></div>
        </div> -->
        <!-- INFORMATION VILLA -->
        <div class="footer">
            <ul class="signature">
              <li class="villaTitre">villa Noailles</li>
              <li>Centre d'Art d'Intérêt National</li>
              <li>Montée Noailles 83400 Hyères</li>
              <li>www.villanoailles-hyeres.com</li>
              <li>T. +33 (0)4 98 08 01 98 / 97</li>
            </ul>
            <div class="logoVille">Communauté d'Agglomération <br>Toulon Provence Méditerranée</div>
        <!-- PAVE LOGO -->
          <div class="logoPave affiche">
            <img src="img/logos_noir.jpg" alt="" />
          </div>
          <p class="luuse">Design&thinsp;: Luuse</p>
        </div>
</div>

<?php
  header('Cache-Control: no-cache, must-revalidate');
  header('Cache-Control: post-check=0, pre-check=0', false);
  header('Pragma: no-cache');
  header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT ')
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Poster Html</title>
  </head>
  <body>
    <ul id="tools">
      <li><input class="hideGrid preview" type="button" name="preview" value="preview"></li>
      <li><input class="hideGrid grids" type="button" name="grids" value="grid"></li>
      <li class="zoom"><input class="rangeValue" min="1" max="40" step="1" value="20" type="range"></li>
      <input class="save" type="button" onclick='SvgSave(".mjson")' value='Layer1 svg' />
      <input class="save" type="button" onclick='SvgSave(".mjson2")' value='Layer2 svg' />
    </ul>

    <div class="content">
    </div>

  </body>


  <div id="content" data-content="content/content-affiche.html?v<?php echo rand(); ?>" ></div>
  <script src="http://cdn.jsdelivr.net/g/filesaver.js?v<?php echo rand(); ?>"></script>
  <script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
  <script charset="utf-8" src="MJ/js/michael-json.js?v<?php echo rand(); ?>"></script>
  <script src="js/less.min.js?v<?php echo rand(); ?>" type="text/javascript"></script>
  <script src="js/jquery.arctext.js?v<?php echo rand(); ?>" type="text/javascript"></script>
  <script src="js/functions.js?v<?php echo rand(); ?>" ></script>
  <script src="js/main.js?v<?php echo rand(); ?>" ></script>
  <script src="js/polyfill.js?v<?php echo rand(); ?>" type="text/javascript"></script>
</html>

<?php
  if (!$pages) {
    $pages = 1;
  }
  if (!$max) {
      # code...
    if ($currentPage == 'concours') {
      $max = 1;
    } elseif ($currentPage == 'expositions') {
      if ($titre == 'ouverture' || $titre == 'Ouverture'){
        $max = 1;
      } else {
        $max = 13;
      }
    } elseif ($currentPage == 'ouverture') {
      $max = 1;
    } elseif ($currentPage == "remerciements") {
      $max = 9;
    }
  }
  for ($i=0; $i <= $max; $i++) {
    if ($i % 2 == 0): ?>

      <div class="bleed bleed<?= $i ?> singlePages">
        <div class="page pageLeft" id="page_<?= $i ?>">
          <div class="crop cropTop"></div>
          <div class="crop cropLeft"></div>
          <?php include('./inc/grid.php'); ?>
            <div class="structure structure0">
              <?php if ($i == 0){ ?>
                <div class="headerTo">
                </div>
                <div class="textTo textToFirst">

                </div>
              <?php } else { ?>
                <div class="textTo">

                </div>
              <?php } ?>
            </div>

            <div class="pagination"><?= $pages ?></div>

          <div class="crop cropRight"></div>
          <div class="crop cropBottom"></div>
        </div>
      </div>
      <?php $pages ++; ?>
      <?php endif; ?>
      <?php if ($i % 2 != 0): ?>

        <div class="bleed bleed<?= $i ?> singlePages">
          <div class="page pageRight" id="page_<?= $i ?>">
            <div class="crop cropTop"></div>
            <div class="crop cropLeft"></div>
            <?php include('./inc/grid.php'); ?>
              <div class="structure structure0">
                <?php if ($currentPage == "remerciements") { ?>
                  <div class="texts">
                    <div class="textTo">                  </div>
                    <div class="textTo">                  </div>
                    <div class="textTo">                  </div>
                  </div>
                <?php } else { ?>
                  <div class="textTo">                  </div>
                <?php } ?>
              </div>

            <div class="pagination"><?= $pages ?></div>

            <div class="crop cropRight"></div>
            <div class="crop cropBottom"></div>
          </div>
        </div>
        <?php $pages ++; ?>
    <?php endif; ?>
    <?php
  }
?>

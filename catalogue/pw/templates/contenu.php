<?php
  include("./inc/head.php");
  $partie = $page;
  $children = $page->parent->children();
  $total = $children->count();
  $pagePos = ($children->getItemKey($page) + 1) * 2;
  $id = $page->id;
  if ($currentPage == 'concours') {
    $incr = 4;
  } else if ($currentPage == 'expositions') {
    $incr = 34;
    if ($id == 1044) { // Niebling
      $incr = 46;
      $max = 9;
    } else if ($id == 1060) { // Hoffner
      $incr = 54;
      $max = 9;
    } else if ($id == 1061) { // Picasso
      $incr = 62;
      $max = 11;
    } else if ($id == 1062) { // Passolunghi
      $incr = 72;
      $max = 11;
    } else if ($id == 1063) { // Reine Jane
      $incr = 82;
      $max = 7;
    } else if ($id == 1064) { // Xenia
      $incr = 88;
      $max = 5;
    } else if ($id == 1065) { // Darré
      $incr = 92;
      $max = 5;
    }
  } else if ($currentPage == 'remerciements') {
    if ($id == 1067 || $id == 1089) { // Remerciements
      $incr = 114;
      $max = 7;
    }
  }
  $pages = $pagePos + $incr;
?>

<section class="part" id="part<?= $partie->id ?>">

  <?php

    $partie->setOutputFormatting(false);

    $artiste = $partie->artiste;
    $paysFr = $partie->pays->getLanguageValue('default');
    $paysEn = $partie->pays->getLanguageValue('en');
    $titre = $partie->title;
    $titreEn = $partie->title->getLanguageValue('en');
    $sous_titreFr = $partie->sous_titre->getLanguageValue('default');
    $sous_titreEn = $partie->sous_titre->getLanguageValue('en');
    $texteFr = $partie->texte_courant->getLanguageValue('default');
    $texteEn = $partie->texte_courant->getLanguageValue('en');
    $bioFr = $partie->infos->getLanguageValue('default');
    $bioEn = $partie->infos->getLanguageValue('en');
    $images = $partie->images;

    include('./pages.php');
    if ($currentPage == 'concours' ){
      include('concours.php');
    } elseif ($currentPage == "expositions") {
      include('expositions.php');
    } elseif ($currentPage == "remerciements") {
      include('remerciements.php');
    }
 ?>

</section>
<?php
  include("./inc/foot.php");
?>

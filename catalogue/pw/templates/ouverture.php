<?php

  include("./inc/head.php");

  $partie = $page;
  $sous_titreFr = $partie->sous_titre;
  $textFr = $partie->texte_courant;
  $images = $partie->images;

  $id = $page->id;
  $max = 1;

  if ($id == 1056 || $id == 1057) { // sommaire hyeres + Toulon
    $pages = 4;
  } else if ($id == 1081 || $id == 1082) { // ouverture concours hyeres + Toulon
    $pages = 6;
  } else if ($id == 1085 || $id == 1087) { // jury hyeres + Toulon
    $pages = 8;
  } else if ($id == 1086 || $id == 1088) { // dotations hyeres + Toulon
    $pages = 10;
  } else if ($id == 1096 || $id == 1101) { // contact Toulon + scéno hyères
    $pages = 32;
  } else if ($id == 1091) { // contact hyres
    $pages = 34;
  } else if ($id == 1097 || $id == 1099) { // rewind hyeres + Toulon
    $pages = 124;
  } else if ($id == 1084 || $id == 1083) { // ouverture expos hyeres + Toulon
    $pages = 34;
  } else if ($id == 1100 || $id == 1098) { // ours hyeres + toutou
    $pages = 126;
  }
?>
<section class="part" id="part<?= $partie->id ?>">

<?php
  include('./pages.php');
  include('./ouvertures.php');
?>
</section>
<?php include("./inc/foot.php"); ?>

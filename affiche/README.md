# DESIGN PARADE -> AFFICHE
Luuse 2018

## Aller d'un fichier à un autre

On change de fichier lorsqu'on change le hash dans l'url.
Exemple:

`print-designparade/affiche/#master-1`
`master` = le nom de la version.
`1` = l'échelle du zoom dans la page.
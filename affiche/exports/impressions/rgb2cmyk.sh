#!/bin/bash

chemin=$1
echo $chemin

for file in $chemin/rgb/*; do
  echo "$(basename "$file")"
  gs \
    -dNOPAUSE \
    -dBATCH \
    -sDEVICE=pdfwrite \
    -dPDFSETTINGS=/prepress \
    -dColorImageResolution=300 \
    -dDownsampleColorImages=false \
    -dProcessColorModel=/DeviceCMYK \
    -sColorConversionStrategy=CMYK \
    -dAutoFilterColorImages=false \
    -dAutoFilterGrayImages=false \
    -dColorImageFilter=/FlateEncode \
    -dGrayImageFilter=/FlateEncode\
    -dDownsampleMonoImages=false \
    -dDownsampleGrayImages=false \
    -dDownsampleColorImages=false\
    -sOutputFile=$chemin/cmyk/"$(basename "$file")" \
    $chemin/rgb/"$(basename "$file")"
done

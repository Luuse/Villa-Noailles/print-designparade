<div class="page">
  <div class="page-inside grid">
      <div class="fond"></div>
    <div class="lettrage">
      <div class="design"></div>
      <div class="parade"></div>
    </div>
    <div class="flow-titredp"></div>
    <div class="flow-titredp2"></div>
    <div class="flow-date"></div>
    <div class="flow-inauguration"></div>
    <!-- <div class="flow&#45;categorie"></div> -->
    <div class="flow-hyeres"></div>
    <div class="flow-toulon"></div>
    <!-- <div class="flow&#45;information.off"></div> -->
    <!-- <div class="flow&#45;information&#45;hyere"></div> -->
    <div class="flow-site"></div>
    <!-- <div class="flow&#45;signature"></div> -->
    <!-- <div class="flow&#45;luuse"></div> -->
    <div class="flow-footer.off"></div>
    <div class="mjson">
    </div>
    <!-- <div class="mjson2"> -->
    <!-- </div> -->

  </div>
</div>

<link rel="stylesheet" type="text/css" href="files/bus/style.css?v<?php rand(); ?>" media="all">
<script charset="utf-8">
  // $('.categorie li .circleinf').arctext({radius: 50, dir: -1,rotate: true});
  $('.categorie li').arctext({radius: 5, dir: 1,rotate: false});
  $('.date span').arctext({radius: 30, dir: 21, rotate: false});
  // $('.inauguration').arctext({radius: 3, dir: 40,rotate: false});
  // $('.ville span').arctext({radius: 10, dir: -1, rotate: false});

  zoomPages($('.range'), $('.page'));
  var orderJson2 = ['timesb-handles-over','timesb-handles-dot-over'];
  // models.json = [];
  lettrine(
    '.lettrage .parade',
    'PARADE',
    2,
    {
      json: orderJson2,
      style: [],
      morph: {
        'transform': ['scale-y', '1', '0.5' ],
  },
    }
  );
  var orderJson1 = ['timesb-handles-over','timesb-handles-dot-over','timesb-over','timesb-handles-dot-over'] ;
  var value = [];
  lettrine(
    '.lettrage .design',
    'DESIGN',
    2,
    {
      json: orderJson1,
      style: [],
      morph: {
        'transform': ['scale-y', '1', '0.5' ],
      },
    }
  );

  var point = 7;
  var blue = '#0047B6';
  var vert = '#5BDD45';
  var rose = '#F8B8CB'; //hyeres
  // var fond = '#FCCC93';
  var fond = '#ede935';
  var font = 'parade';

  // MichaelJson({
  //   jsonFile:'echant-simple',
  //   container: '.mjson',
  //   font: font,
  //   glyphs: ['W', 'Y', 'v'],
  //   findColor: '#ffffff0',
  //   glyphColor: [vert],
  //   freq: [1,20],
  //   rotate: 10,
  //   translateX: 0,
  //   incTransX: 0,
  //   translateY: 0,
  //   incTransY: 0,
  //   scan: ['all'], // interval example ´scan: [10, 50]´
  //   incStyle: [
  //     ['font-size', 5, 'pt', 0],
  //   ]
  // });
  //// C E N T E R
  MichaelJson({
    jsonFile:'echant-simple',
    container: '.mjson',
    font: font,
    glyphs: ['W', 'Y', 'v'],
    findColor: '#ff0202',
    glyphColor: ['white', vert],
    freq: [2,3],
    rotate: 20,
    translateX: 0,
    incTransX: 0.01,
    translateY: 0,
    incTransY: 0,
    scan: ['all'], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', 5, 'pt', 0],
    ]
  });
  MichaelJson({
    jsonFile:'echant-simple',
    container: '.mjson',
    font: font,
    glyphs: ['W', 'Y', 'v'],
    findColor: '#020202',
    glyphColor: ['white', vert, vert],
    freq: [2,3],
    rotate: 10,
    translateX: 0,
    incTransX: 0.01,
    translateY: 0,
    incTransY: 0,
    scan: ['all'], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', 5, 'pt', 0],
    ]
  });
  
  MichaelJson({
    jsonFile:'echant-simple',
    container: '.mjson',
    font: font,
    glyphs: ['W', 'Y', 'v'],
    findColor: '#ffff02',
    glyphColor: [ fond, fond, rose],
    freq: [1,3],
    rotate: 0,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: ['all'], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', 5, 'pt', 0],
    ]
  });
  MichaelJson({
    jsonFile:'echant-simple',
    container: '.mjson',
    font: font,
    glyphs: ['W', 'Y', 'v'],
    findColor: '#0202ff',
    glyphColor: [ fond, rose, rose ],
    freq: [1,3],
    rotate: 0,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: ['all'], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', 5, 'pt', 0],
    ]
  });

</script>

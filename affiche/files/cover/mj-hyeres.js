 MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson2',
    font: font,
    glyphs: ['Z', '['],
    findColor: '#0202ff',
    glyphColor: [vert, 'white', vert],
    freq: [1,1],
    rotate: 10,
    translateX: 1,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [0, 110], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  // L2
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson',
    font: font,
    glyphs: ['}', 'b', 'c'],
    findColor: '#0202ff',
    glyphColor: [vert, 'white', fond],
    freq: [1,1],
    rotate: -30,
    translateX: 10,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [0, 110], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  // L E F T 
  // 1B
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson2',
    font: font,
    glyphs: ['e', 'x', 'f'],
    findColor: '#ff0202',
    glyphColor: ['white'],
    freq: [1,2],
    rotate: 0,
    translateX: 10,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [110, 220], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  // L E F T 
  // 1C
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson',
    font: font,
    glyphs: ['W', 'Y', 'v'],
    findColor: '#ff0202',
    glyphColor: ['white', 'white', vert],
    freq: [1,1],
    rotate: 15,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [220, 330], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });









  // C E N T E R 
  // 2A
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson',
    font: font,
    glyphs: ['W', 'Y', 'v'],
    findColor: '#020202',
    glyphColor: [vert, 'white', fond, 'white'],
    freq: [1,1],
    rotate: 0,
    translateX: 10,
    incTransX: 0.005,
    translateY: 0,
    incTransY: 0,
    scan: [0, 50], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson2',
    font: font,
    glyphs: ['W', 'Y', 'v'],
    findColor: '#020202',
    glyphColor: [vert, vert,  fond, 'white'],
    freq: [1,1],
    rotate: 0,
    translateX: 20,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [0, 110], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  
  // C E N T E R
  // 2B
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson',
    font: font,
    glyphs: ['W', 'Y', 'v'],
    findColor: '#020202',
    glyphColor: ['white'],
    freq: [1,1],
    rotate: -90,
    translateX: 10,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [110, 220], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson2',
    font: font,
    glyphs: ['k', '~', 'o', 'q'],
    findColor: '#020202',
    glyphColor: ['white'],
    freq: [1,2],
    rotate: 90,
    translateX: 0,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [110, 220], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  
  // C E N T E R
  // 2C
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson',
    font: font,
    glyphColor: ['white'],
    glyphs: ['W', 'Y', 'v'],
    findColor: '#020202',
    glyphColor: ['white'],
    freq: [1,2],
    rotate: 0,
    translateX: 10,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [220, 330], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });

  // R I G T H  
  // 3A
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson',
    font: font,
    glyphs: ['g', 'h', 'm', 'j'],
    findColor: '#ff0202',
    glyphColor: ['white', 'white', 'white', vert, fond],
    freq: [1,2],
    rotate: -10,
    translateX: 10,
    incTransX: 0,
    translateY: 0,
    incTransY: 0.001,
    scan: [0, 110], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson2',
    font: font,
    glyphs: ['g', 'h', 'm', 'j'],
    findColor: '#ff0202',
    glyphColor: [ vert, fond],
    freq: [1,20],
    rotate: -10,
    translateX: 10,
    incTransX: 0,
    translateY: 0,
    incTransY: 0.001,
    scan: [0, 110], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });

  // R I G T H  
  // 3B
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson',
    font: font,
    glyphs: ['t', 'n', 'd', 'w',],
    findColor: '#0202ff',
    glyphColor: ['white', fond, 'white', fond, vert],
    freq: [1,2],
    rotate: 0,
    translateX: 10,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [110, 220], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });

  // R I G T H  
  // 3C
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson',
    font: font,
    glyphs: ['W', 'Y', 'v'],
    findColor: '#0202ff',
    glyphColor: ['white'],
    freq: [1,6],
    rotate: 0,
    translateX: 10,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [220, 330], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
  MichaelJson({
    jsonFile: 'echant-cover',
    container: '.mjson2',
    font: font,
    glyphs: ['À', 'p'],
    findColor: '#0202ff',
    glyphColor: [fond, fond, 'white'],
    freq: [1,3],
    rotate: 0,
    translateX: 10,
    incTransX: 0,
    translateY: 0,
    incTransY: 0,
    scan: [220, 330], // interval example ´scan: [10, 50]´
    incStyle: [
      ['font-size', point, 'pt', 0],
    ]
  });
